/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package application

import (
	"context"
	"fmt"

	"gitlab.com/slax0rr/go-beer-api/domain"
)

type Manufacturer interface {
	Create(context.Context, *domain.Manufacturer) error
	Update(context.Context, *domain.Manufacturer) error
	Delete(context.Context, int64) error
	List(context.Context) ([]domain.Manufacturer, error)
	GetByID(context.Context, int64) (*domain.Manufacturer, error)
}

// Manufacturer application layer between the interface and the domain implementatino of manufacturer.
type ManufacturerImpl struct{}

// Compile-time check if ManufacturerImpl imlpements Manufacturer
var _ Manufacturer = new(ManufacturerImpl)

// Create a new manufacturer object.
func (a *ManufacturerImpl) Create(ctx context.Context, manufacturer *domain.Manufacturer) error {
	if manufacturer == nil {
		return fmt.Errorf("manufacturer entity may not be nil")
	}

	return manufacturer.Save(ctx)
}

// Update an existing manufacturer object.
func (a *ManufacturerImpl) Update(ctx context.Context, manufacturer *domain.Manufacturer) error {
	if manufacturer == nil {
		return fmt.Errorf("manufacturer entity may not be nil")
	}

	if manufacturer.ID == 0 {
		return ErrZeroID
	}

	return manufacturer.Save(ctx)
}

// Delete an existing manufacturer object.
func (a *ManufacturerImpl) Delete(ctx context.Context, id int64) error {
	manufacturer := new(domain.Manufacturer)
	manufacturer.ID = id

	return manufacturer.Delete(ctx)
}

// List obtains a list of manufacturers from the domain and returns them.
func (a *ManufacturerImpl) List(ctx context.Context) ([]domain.Manufacturer, error) {
	return domain.GetManufacturerRepo().Get(ctx)
}

// GetByID obtains a single manufacturer with the specified `id` and returns it.
func (a *ManufacturerImpl) GetByID(ctx context.Context, id int64) (*domain.Manufacturer, error) {
	manufacturer := new(domain.Manufacturer)
	manufacturer.ID = id

	err := manufacturer.Get(ctx)
	if err != nil {
		return nil, err
	}

	return manufacturer, nil
}

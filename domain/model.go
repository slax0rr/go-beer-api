/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package domain

import (
	"context"
	"time"
)

type Model struct {
	CreatedAt time.Time `json:"-" pg:",notnull"`
	UpdatedAt time.Time `json:"-"`
}

// BeforeInsert is executed before the object is persisted using the Insert method of the database database object.
func (m *Model) BeforeInsert(ctx context.Context) (context.Context, error) {
	m.CreatedAt = time.Now().UTC()
	return ctx, nil
}

// BeforeUpdate is executed before the object is persisted using the Update method of the database database object.
func (m *Model) BeforeUpdate(ctx context.Context) (context.Context, error) {
	m.UpdatedAt = time.Now().UTC()
	return ctx, nil
}

type TransactionalRepo interface {
	Begin(context.Context) (context.Context, error)
	Commit(context.Context) (context.Context, error)
	Rollback(context.Context) (context.Context, error)
}

/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package database

import (
	"context"
	"fmt"

	"github.com/go-pg/pg/v9"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
)

type contextKey string

func (key contextKey) String() string {
	return "slax0rr/database context key " + string(key)
}

const transactionCtxKey = contextKey("transaction")

// base persistance object including the database connection instance.
type base struct {
	db pgwrapper.DB
}

// getDB returns the *pg.DB with the current context, or the transaction from the context if it implements the `DB` interface.
func (t *base) getDB(ctx context.Context) pgwrapper.DB {
	var db pgwrapper.DB
	db = t.db.WithContext(ctx)
	tx, ok := ctx.Value(transactionCtxKey).(pgwrapper.DB)
	if ok {
		db = tx
	}

	return db
}

// Begin starts a new transaction with the received context, creates a new context with the transaction as a value and returns it.
func (t *base) Begin(ctx context.Context) (context.Context, error) {
	tx, err := t.db.WithContext(ctx).Begin()
	if err != nil {
		return ctx, err
	}

	return context.WithValue(ctx, transactionCtxKey, tx), nil
}

// Commit the transaction.
// If the transaction is not found in the context, and error is returned.
func (t *base) Commit(ctx context.Context) (context.Context, error) {
	tx, ok := ctx.Value(transactionCtxKey).(pgwrapper.Tx)
	if !ok {
		return ctx, fmt.Errorf("unable to execute commit, no transaction found")
	}

	err := tx.Commit()
	if err != nil {
		return ctx, err
	}

	return context.WithValue(ctx, transactionCtxKey, nil), nil
}

// Rollback the transaction.
// If the transaction is not found in the context, and error is returned.
func (t *base) Rollback(ctx context.Context) (context.Context, error) {
	tx, ok := ctx.Value(transactionCtxKey).(pgwrapper.Tx)
	if !ok {
		return ctx, fmt.Errorf("unable to execute rollback, no transaction found")
	}

	err := tx.Rollback()
	if err != nil {
		return ctx, err
	}

	return context.WithValue(ctx, transactionCtxKey, nil), nil
}

// parseError parses the `pg` error and returns an object of `Error`.
// PostgreSQL error 23503 - `ErrForeignKeyConstraint`
func parseError(err error) error {
	switch e := err.(type) {
	case pg.Error:
		switch e.Field('C') {
		case "23503":
			persErr := ErrForeignKeyConstraint
			persErr.Details = map[string]string{
				"table":      e.Field('t'),
				"constraint": e.Field('n'),
			}
			return persErr
		}
	}

	return err
}

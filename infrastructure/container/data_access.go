/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package container

import (
	"crypto/tls"
	"fmt"
	"time"

	"github.com/go-pg/pg/v9"
	"github.com/spf13/viper"
	"gitlab.com/slax0rr/go-beer-api/domain"
	"gitlab.com/slax0rr/go-beer-api/infrastructure/database"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
)

var db pgwrapper.DB

// InitDB initializes the database connection using the connection data from the config.
func InitDB() error {
	var tlsConfig *tls.Config
	switch viper.GetString("db.sslmode") {
	case "verify-ca", "verify-full":
		tlsConfig = &tls.Config{}
	case "allow", "prefer", "require":
		tlsConfig = &tls.Config{InsecureSkipVerify: true} //nolint
	case "disable":
		tlsConfig = nil
	default:
		tlsConfig = &tls.Config{InsecureSkipVerify: true}
	}

	db = pgwrapper.NewDB(pg.Connect(&pg.Options{
		Network:     viper.GetString("db.network"),
		Addr:        fmt.Sprintf("%s:%d", viper.GetString("db.host"), viper.GetInt("db.port")),
		User:        viper.GetString("db.user"),
		Password:    viper.GetString("db.pass"),
		Database:    viper.GetString("db.name"),
		DialTimeout: viper.GetDuration("db.timeout") * time.Second,
		TLSConfig:   tlsConfig,
	}))

	_, err := db.Exec("SELECT 1")
	if err != nil {
		return err
	}

	return nil
}

// InitRepositories injects the database objects to the repositories.
func InitRepositories() {
	domain.InitManufacturerRepo(database.NewManufacturer(db))
	domain.InitBeerRepo(database.NewBeer(db))
	domain.InitAddressRepo(database.NewAddress(db))
}

// GetDB returns the database instance
func GetDB() pgwrapper.DB {
	return db
}

// CloseDB closes the database connection.
func CloseDB() error {
	return db.Close()
}

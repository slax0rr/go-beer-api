/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package beerapi

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/go-pg/pg/v9/orm"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	httpserver "github.com/slax0rr/go-httpserver"
	"github.com/spf13/viper"
	"gitlab.com/slax0rr/go-beer-api/domain"
	"gitlab.com/slax0rr/go-beer-api/infrastructure/container"
)

// Run starts the application by initializing the database connection and starting the web server.
func Run() {
	if viper.GetBool("db.enabled") {
		if err := container.InitDB(); err != nil {
			log.WithError(err).Fatal("unable to open connection to the database")
		}
		defer container.CloseDB()

		container.InitRepositories()
	}

	if err := serve(); err != nil {
		log.WithError(err).Fatal("http server unable to start or stopped abruptly")
	}
}

// BuildSchema creates the database schema from the domain entities.
func BuildSchema() {
	if !viper.GetBool("db.enabled") {
		log.Error("unable to build schema, database is not enabled")
		os.Exit(1)
	}

	if err := container.InitDB(); err != nil {
		log.WithError(err).Fatal("unable to open connection to the database")
	}
	defer container.CloseDB()

	db := container.GetDB()
	if db == nil {
		log.Fatal("unable to obtain a valid db connection")
	}

	tx, err := db.Begin()
	if err != nil {
		log.WithError(err).Error("unable to start a new transaction")
	}
	defer func() {
		if err := tx.Close(); err != nil {
			log.WithError(err).Error("unable to close transaction")
		}
	}()

	models := []interface{}{
		(*domain.Country)(nil),
		(*domain.City)(nil),
		(*domain.Manufacturer)(nil),
		(*domain.Beer)(nil),
	}

	opts := &orm.CreateTableOptions{IfNotExists: true, FKConstraints: true}

	for _, m := range models {
		err := tx.CreateTable(m, opts)
		if err != nil {
			log.WithError(err).Fatal("unable to create schema")
		}
	}

	if err := tx.Commit(); err != nil {
		log.WithError(err).Fatal("unable to commit transaction")
	}
}

type Handler interface {
	Register(*mux.Router)
}

// serve starts the HTTP server and registers handlers.
func serve() error {
	rtr := mux.NewRouter()

	if viper.GetBool("http.healthcheck") {
		// simple healthcheck route, returning 200 signaling the HTTP server has started up
		rtr.HandleFunc("/healthcheck",
			func(w http.ResponseWriter, req *http.Request) {})
	}

	handlers := container.GetHTTPHandlers()
	for _, h := range handlers {
		handler, ok := h.(Handler)
		if !ok {
			logrus.Fatalf("handler (%v) does not implement the 'Handler' interface", h)
		}
		handler.Register(rtr)
	}

	return httpserver.Serve(httpserver.Config{
		SockFile: viper.GetString("http.sockFile"),
		Addr: fmt.Sprintf("%s:%d", viper.GetString("http.host"),
			viper.GetInt("http.port")),
		Timeout: time.Duration(viper.GetInt("http.timeout")) * time.Second,
	}, rtr)
}

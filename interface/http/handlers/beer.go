/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package handlers

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/go-beer-api/application"
	"gitlab.com/slax0rr/go-beer-api/domain"
	"gitlab.com/slax0rr/go-beer-api/infrastructure/database"
	httphelper "gitlab.com/slax0rr/go-beer-api/interface/http"
)

type Beer struct {
	beerApp application.Beer
	http    httphelper.HTTPHelper
}

// NewBeerHandler is the factory for creating the Beer handler.
func NewBeerHandler(
	beerApp application.Beer,
	http httphelper.HTTPHelper,
) *Beer {
	return &Beer{beerApp, http}
}

// Register registers the handler endpoints to the router.
func (h *Beer) Register(rtr *mux.Router) {
	r := rtr.PathPrefix("/beer").Subrouter()

	r.Path("").HandlerFunc(h.Create).Methods(http.MethodPost)
	r.Path("").HandlerFunc(h.List).Methods(http.MethodGet)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Get).Methods(http.MethodGet)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Update).Methods(http.MethodPut)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Delete).Methods(http.MethodDelete)
}

// Create handles the POST /beer/ endpoint by parsing the incoming request and passintg the `domain.Beer` entity object on to the BeerApp.
func (h *Beer) Create(w http.ResponseWriter, req *http.Request) {
	beer := new(domain.Beer)
	if err := h.http.ParseRequest(w, req, beer); err != nil {
		return
	}

	err := h.beerApp.Create(req.Context(), beer)
	if err != nil {
		logrus.WithError(err).Error("unable to create a new beer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}

	h.http.SendResponse(w, beer, http.StatusCreated)
}

// List handles the GET /beer endpoint by providing a list of all available `domain.Beer` objects.
func (h *Beer) List(w http.ResponseWriter, req *http.Request) {
	list, err := h.beerApp.List(req.Context())
	if err != nil {
		logrus.WithError(err).Error("unable to obtain a list of beers")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}

	h.http.SendResponse(w, list, http.StatusOK)
}

// Get handles the GET /beer/{id} endpoint by obtaining the requested `domain.Beer` object.
func (h *Beer) Get(w http.ResponseWriter, req *http.Request) {
	// an error can not occur, since the regex in the path already limits to positive numbers
	id, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	beer, err := h.beerApp.GetByID(req.Context(), id)
	switch {
	case err == nil:
		h.http.SendResponse(w, beer, http.StatusOK)

	case err.Error() == database.ErrRowNotFound.Error():
		logrus.WithField("error", err).Info("requested beer not found")
		h.http.SendError(w, httphelper.ErrNotFound)
		return

	default:
		logrus.WithError(err).WithField("id", id).Error("unable to obtain the beer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

// Update handles the PUT /beer/{id} endpoint by updating the `domain.Beer` object.
func (h *Beer) Update(w http.ResponseWriter, req *http.Request) {
	beer := new(domain.Beer)
	if err := h.http.ParseRequest(w, req, beer); err != nil {
		return
	}

	// an error can not occur, since the regex in the path already limits to positive numbers
	beer.ID, _ = strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	err := h.beerApp.Update(req.Context(), beer)
	switch {
	case err == nil:
		h.http.SendResponse(w, beer, http.StatusOK)

	case err.Error() == database.ErrRowNotFound.Error():
		logrus.WithField("error", err).Info("requested beer not found")
		h.http.SendError(w, httphelper.ErrNotFound)
		return

	default:
		logrus.WithError(err).
			WithField("id", beer.ID).
			Error("unable to update the beer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

// Delete handles the DELETE /beer/{id} endpoint by removing the `domain.Beer` object.
func (h *Beer) Delete(w http.ResponseWriter, req *http.Request) {
	// an error can not occur, since the regex in the path already limits to positive numbers
	id, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	err := h.beerApp.Delete(req.Context(), id)
	switch {
	case err == nil:
		fallthrough

	case err.Error() == database.ErrRowNotFound.Error():
		h.http.SendResponse(w, nil, http.StatusNoContent)

	case err.Error() == database.ErrForeignKeyConstraint.Error():
		logrus.WithError(err).
			WithField("id", id).
			Error("unable to delete the beer")
		respErr := httphelper.ErrConflict
		respErr.Details = err
		h.http.SendError(w, respErr)

	default:
		logrus.WithError(err).
			WithField("id", id).
			Error("unable to delete the beer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package handlers_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slax0rr/go-beer-api/domain"
	"gitlab.com/slax0rr/go-beer-api/infrastructure/database"
	httphelper "gitlab.com/slax0rr/go-beer-api/interface/http"
	"gitlab.com/slax0rr/go-beer-api/interface/http/handlers"
	mockobj "gitlab.com/slax0rr/go-beer-api/test/mock"
)

type ManufacturerTestSuite struct {
	suite.Suite

	rtr                 *mux.Router
	manufacturerApp     *mockobj.Manufacturer
	http                *mockobj.HTTPHelper
	manufacturerHandler *handlers.Manufacturer
}

func (s *ManufacturerTestSuite) SetupTest() {
	s.manufacturerApp = new(mockobj.Manufacturer)
	s.http = new(mockobj.HTTPHelper)
	s.manufacturerHandler = handlers.NewManufacturerHandler(s.manufacturerApp, s.http)

	s.rtr = mux.NewRouter()
	s.manufacturerHandler.Register(s.rtr)
}

func (s *ManufacturerTestSuite) TestCreate() {
	testCases := []struct {
		input     *domain.Manufacturer
		output    *domain.Manufacturer
		code      int
		parseErr  error
		createErr error
	}{
		{
			&domain.Manufacturer{
				Name:    "foo",
				Street1: "bar 15",
				ZipCode: "1234",
				CityID:  1,
			},
			&domain.Manufacturer{
				ID:      1,
				Name:    "foo",
				Street1: "bar 15",
				ZipCode: "1234",
				CityID:  1,
			},
			http.StatusCreated,
			nil,
			nil,
		},
		{
			&domain.Manufacturer{},
			nil,
			http.StatusInternalServerError,
			nil,
			fmt.Errorf("test create error"),
		},
		{
			&domain.Manufacturer{},
			nil,
			http.StatusUnprocessableEntity,
			fmt.Errorf("test parse error"),
			nil,
		},
	}

	mockParseReq := s.http.On(
		"ParseRequest",
		mock.AnythingOfType("*httptest.ResponseRecorder"),
		mock.AnythingOfType("*http.Request"),
		mock.AnythingOfType("*domain.Manufacturer"),
	)

	for _, tc := range testCases {
		b, err := json.Marshal(tc.input)
		if err != nil {
			s.T().Fatalf("unable to marshal test input: %s", err.Error())
		}

		r := bytes.NewBuffer(b)
		rec := httptest.NewRecorder()
		req, err := http.NewRequest("POST", "/manufacturer", r)
		if err != nil {
			s.T().Fatalf("unable to create test request: %s", err.Error())
		}

		mockParseReq.Run(func(args mock.Arguments) {
			if tc.parseErr == nil {
				m := args.Get(2).(*domain.Manufacturer)
				*m = *tc.input
			}
		}).Return(tc.parseErr)

		s.manufacturerApp.On("Create", mock.AnythingOfType("*context.valueCtx"), tc.input).
			Run(func(args mock.Arguments) {
				if tc.createErr == nil {
					m := args.Get(1).(*domain.Manufacturer)
					*m = *tc.output
				}
			}).Return(tc.createErr)

		if tc.createErr == nil && tc.parseErr == nil {
			s.http.On("SendResponse", rec, tc.output, tc.code).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendResponse", rec, tc.output, tc.code)
		} else if tc.parseErr == nil {
			s.http.On("SendError", rec, httphelper.ErrInternalServerError).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrInternalServerError)
		} else {
			s.rtr.ServeHTTP(rec, req)
		}
	}

	s.http.AssertNumberOfCalls(s.T(), "SendResponse", 1)
	s.http.AssertNumberOfCalls(s.T(), "SendError", 1)
}

func (s *ManufacturerTestSuite) TestList() {
	testCases := []struct {
		output  []domain.Manufacturer
		code    int
		listErr error
	}{
		{
			[]domain.Manufacturer{
				{
					ID:      1,
					Name:    "foo",
					Street1: "bar 15",
					ZipCode: "1234",
					CityID:  1,
				},
			},
			http.StatusOK,
			nil,
		},
		{
			nil,
			http.StatusInternalServerError,
			fmt.Errorf("test error"),
		},
	}

	listMock := s.manufacturerApp.On("List", mock.AnythingOfType("*context.valueCtx"))

	for _, tc := range testCases {
		rec := httptest.NewRecorder()
		req, err := http.NewRequest("GET", "/manufacturer", nil)
		if err != nil {
			s.T().Fatalf("unable to create test request: %s", err.Error())
		}

		listMock.Return(tc.output, tc.listErr).Once()

		if tc.listErr == nil {
			s.http.On("SendResponse", rec, tc.output, tc.code).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendResponse", rec, tc.output, tc.code)
		} else {
			s.http.On("SendError", rec, httphelper.ErrInternalServerError).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrInternalServerError)
		}
	}
}

func (s *ManufacturerTestSuite) TestGet() {
	testCases := []struct {
		id     int64
		output *domain.Manufacturer
		code   int
		getErr error
	}{
		{
			1,
			&domain.Manufacturer{
				ID:      1,
				Name:    "foo",
				Street1: "bar 15",
				ZipCode: "1234",
				CityID:  1,
			},
			http.StatusOK,
			nil,
		},
		{
			2,
			nil,
			http.StatusInternalServerError,
			fmt.Errorf("test get error"),
		},
		{
			3,
			nil,
			http.StatusNotFound,
			database.ErrRowNotFound,
		},
	}

	for _, tc := range testCases {
		rec := httptest.NewRecorder()
		req, err := http.NewRequest("GET", fmt.Sprintf("/manufacturer/%d", tc.id), nil)
		if err != nil {
			s.T().Fatalf("unable to create test request: %s", err.Error())
		}

		s.manufacturerApp.On("GetByID", mock.AnythingOfType("*context.valueCtx"), tc.id).Return(tc.output, tc.getErr)

		if tc.getErr == nil {
			s.http.On("SendResponse", rec, tc.output, tc.code).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendResponse", rec, tc.output, tc.code)
		} else if tc.getErr == database.ErrRowNotFound {
			s.http.On("SendError", rec, httphelper.ErrNotFound).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrNotFound)
		} else {
			s.http.On("SendError", rec, httphelper.ErrInternalServerError).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrInternalServerError)
		}
	}
}

func (s *ManufacturerTestSuite) TestUpdate() {
	testCases := []struct {
		input     *domain.Manufacturer
		code      int
		parseErr  error
		updateErr error
	}{
		{
			&domain.Manufacturer{
				ID:      1,
				Name:    "foo",
				Street1: "bar 15",
				ZipCode: "1234",
				CityID:  1,
			},
			http.StatusOK,
			nil,
			nil,
		},
		{
			&domain.Manufacturer{ID: 2},
			http.StatusBadRequest,
			fmt.Errorf("test parse error"),
			nil,
		},
		{
			&domain.Manufacturer{ID: 3},
			http.StatusNotFound,
			nil,
			database.ErrRowNotFound,
		},
		{
			&domain.Manufacturer{ID: 4},
			http.StatusNotFound,
			nil,
			fmt.Errorf("test update error"),
		},
	}

	mockParseReq := s.http.On(
		"ParseRequest",
		mock.AnythingOfType("*httptest.ResponseRecorder"),
		mock.AnythingOfType("*http.Request"),
		mock.AnythingOfType("*domain.Manufacturer"),
	)

	for _, tc := range testCases {
		b, err := json.Marshal(tc.input)
		if err != nil {
			s.T().Fatalf("unable to marshal test input: %s", err.Error())
		}

		r := bytes.NewBuffer(b)
		rec := httptest.NewRecorder()
		req, err := http.NewRequest("PUT", fmt.Sprintf("/manufacturer/%d", tc.input.ID), r)
		if err != nil {
			s.T().Fatalf("unable to create test request: %s", err.Error())
		}

		mockParseReq.Run(func(args mock.Arguments) {
			if tc.parseErr == nil {
				m := args.Get(2).(*domain.Manufacturer)
				*m = *tc.input
			}
		}).Return(tc.parseErr)

		s.manufacturerApp.On("Update", mock.AnythingOfType("*context.valueCtx"), tc.input).Return(tc.updateErr)

		if tc.updateErr == nil && tc.parseErr == nil {
			s.http.On("SendResponse", rec, tc.input, tc.code).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendResponse", rec, tc.input, tc.code)
		} else if tc.updateErr == database.ErrRowNotFound {
			s.http.On("SendError", rec, httphelper.ErrNotFound).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrNotFound)
		} else if tc.parseErr == nil {
			s.http.On("SendError", rec, httphelper.ErrInternalServerError).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrInternalServerError)
		} else {
			s.rtr.ServeHTTP(rec, req)
		}
	}

	s.http.AssertNumberOfCalls(s.T(), "SendResponse", 1)
	s.http.AssertNumberOfCalls(s.T(), "SendError", 2)
}

func (s *ManufacturerTestSuite) TestDelete() {
	testCases := []struct {
		input     int64
		code      int
		deleteErr error
	}{
		{
			1,
			http.StatusNoContent,
			nil,
		},
		{
			2,
			http.StatusNoContent,
			database.ErrRowNotFound,
		},
		{
			3,
			http.StatusInternalServerError,
			fmt.Errorf("test delete error"),
		},
	}

	for _, tc := range testCases {
		rec := httptest.NewRecorder()
		req, err := http.NewRequest("DELETE", fmt.Sprintf("/manufacturer/%d", tc.input), nil)
		if err != nil {
			s.T().Fatalf("unable to create test request: %s", err.Error())
		}

		s.manufacturerApp.On("Delete", mock.AnythingOfType("*context.valueCtx"), tc.input).Return(tc.deleteErr)

		switch tc.deleteErr {
		case nil:
			fallthrough

		case database.ErrRowNotFound:
			s.http.On("SendResponse", rec, nil, tc.code).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendResponse", rec, nil, tc.code)

		default:
			s.http.On("SendError", rec, httphelper.ErrInternalServerError).Return(nil)

			s.rtr.ServeHTTP(rec, req)

			s.http.AssertCalled(s.T(), "SendError", rec, httphelper.ErrInternalServerError)
		}
	}

	s.http.AssertNumberOfCalls(s.T(), "SendResponse", 2)
	s.http.AssertNumberOfCalls(s.T(), "SendError", 1)

}

func TestManufacturerTestSuite(t *testing.T) {
	suite.Run(t, new(ManufacturerTestSuite))
}

/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package domain

import "context"

type City struct {
	tableName struct{} `pg:"city"`

	ID          int64    `json:"id" pg:",pk,notnull"`
	Name        string   `json:"name" pg:",notnull" validate:"required"`
	CountryISO2 string   `json:"country_iso2,omitempty" pg:",notnull" validate:"required_without=Country"`
	Country     *Country `json:"country,omitempty" pg:",notnull" validate:"required_without=CountryISO2"`

	Model
}

func (c *City) Save(ctx context.Context) error {
	return addressRepoImpl.Create(ctx, c)
}

type Country struct {
	tableName struct{} `pg:"country"`

	ISO2 string `json:"iso2" pg:",pk,notnull" validate:"required"`
	ISO3 string `json:"iso3" pg:",notnull" validate:"required"`
	Name string `json:"name" pg:",notnull" validate:"required"`

	Model
}

// AddressRepo lists required methods that the database object must implement in order to be usable by the Address value objects.
type AddressRepo interface {
	TransactionalRepo

	Create(context.Context, *City) error
	Get(context.Context) ([]City, error)
	GetByCityID(context.Context, int64) (*City, error)
}

var addressRepoImpl AddressRepo

// InitAddressRepo sets the database object that implements the AddressRepo interface as the current implementation used for database of the Address value object.
func InitAddressRepo(impl AddressRepo) {
	addressRepoImpl = impl
}

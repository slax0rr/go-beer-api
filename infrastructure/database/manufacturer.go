/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package database

import (
	"context"
	"strconv"

	"gitlab.com/slax0rr/go-beer-api/domain"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
)

// Manufacturer database struct, holding the database connection instance.
type Manufacturer struct {
	*base
}

// NewManufacturer records the database connection instance to the new Manufacturer database object and returns it.
func NewManufacturer(db pgwrapper.DB) *Manufacturer {
	return &Manufacturer{&base{db}}
}

// Create a new Manufacturer in the database table.
func (p *Manufacturer) Create(ctx context.Context, m *domain.Manufacturer) error {
	_, err := p.getDB(ctx).Model(m).Returning("id").Insert()
	if err != nil {
		return err
	}

	return nil
}

// Update the manufacturer record in the database.
// If the manufacturer is not found the `ErrRowNotFound` error is returned.
func (p *Manufacturer) Update(ctx context.Context, m *domain.Manufacturer) error {
	res, err := p.getDB(ctx).Model(m).
		Column("name", "street1", "street2", "zip_code", "city_id", "updated_at").
		WherePK().
		Update()
	if err != nil {
		return err
	}

	if res.RowsAffected() == 0 {
		persErr := ErrRowNotFound
		persErr.Details = struct{ ID int64 }{m.ID}
		return persErr
	}

	return nil
}

// Delete the manufacturer record from the database.
func (p *Manufacturer) Delete(ctx context.Context, m *domain.Manufacturer) error {
	res, err := p.getDB(ctx).Model(m).WherePK().Delete()
	switch e := parseError(err).(type) {
	case nil:

	case Error:
		e.Details.(map[string]string)["id"] = strconv.FormatInt(m.ID, 10)
		return e

	default:
		return e
	}
	if err != nil {
		return parseError(err)
	}

	if res.RowsAffected() == 0 {
		persErr := ErrRowNotFound
		persErr.Details = struct{ ID int64 }{m.ID}
		return persErr
	}

	return nil
}

// Get all manufacturers from the database.
func (p *Manufacturer) Get(ctx context.Context) ([]domain.Manufacturer, error) {
	var manufacturers []domain.Manufacturer

	err := p.getDB(ctx).Model(&manufacturers).Select()
	if err != nil {
		return nil, err
	}

	return manufacturers, nil
}

// GetByID returns a single manufacturer from the database.
func (p *Manufacturer) GetByID(ctx context.Context, id int64) (*domain.Manufacturer, error) {
	m := new(domain.Manufacturer)

	err := p.getDB(ctx).Model(m).
		Relation("City").
		Relation("City.Country").
		Where("manufacturer.id = ?", id).
		Select()
	switch e := parseError(err).(type) {
	case nil:
		return m, nil

	case Error:
		e.Details.(map[string]string)["id"] = strconv.FormatInt(id, 10)
		return nil, e

	default:
		return nil, e
	}
}

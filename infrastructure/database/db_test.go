/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package database

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
	"gitlab.com/slax0rr/go-pg-wrapper/mocks"
)

type testRepo struct {
	*base
}

type baseTestSuite struct {
	suite.Suite

	testRepo *testRepo
	db       *mocks.DB
}

func (s *baseTestSuite) SetupTest() {
	s.db = new(mocks.DB)
	s.testRepo = &testRepo{&base{s.db}}
}

func (s *baseTestSuite) TestBegin() {
	ctx := context.Background()
	tx := new(mocks.Tx)

	s.db.On("WithContext", ctx).Return(s.db)
	beginExp := s.db.On("Begin")
	beginExp.Return(tx, nil)

	// transaction begun
	ctx, err := s.testRepo.Begin(ctx)
	assert.Nil(s.T(), err)
	assert.Equal(s.T(), tx, ctx.Value(transactionCtxKey).(pgwrapper.Tx))

	testErr := fmt.Errorf("test error")
	s.db.On("WithContext", ctx).Return(s.db)
	beginExp.Return(nil, testErr)

	// transaction failed to begin
	ctx, err = s.testRepo.Begin(ctx)
	assert.Equal(s.T(), testErr, err)
	assert.Equal(s.T(), tx, ctx.Value(transactionCtxKey).(pgwrapper.Tx))
}

func (s *baseTestSuite) TestCommit() {
	// transaction is not started, expect an error
	ctx := context.Background()
	ctx, err := s.testRepo.Commit(ctx)
	assert.NotNil(s.T(), err)

	// transaction is commited and unset from context
	tx := new(mocks.Tx)
	ctx = context.WithValue(ctx, transactionCtxKey, tx)
	txMock := tx.On("Commit")
	txMock.Return(nil)
	ctx, err = s.testRepo.Commit(ctx)
	assert.Nil(s.T(), err)
	assert.Nil(s.T(), ctx.Value(transactionCtxKey))

	// failed to commit
	ctx = context.WithValue(ctx, transactionCtxKey, tx)
	txMock.Return(fmt.Errorf("test error"))
	ctx, err = s.testRepo.Commit(ctx)
	assert.NotNil(s.T(), err)
}

func (s *baseTestSuite) TestRollback() {
	// transaction is not started, expect an error
	ctx := context.Background()
	ctx, err := s.testRepo.Rollback(ctx)
	assert.NotNil(s.T(), err)

	// transaction is rolled back and unset from context
	tx := new(mocks.Tx)
	ctx = context.WithValue(ctx, transactionCtxKey, tx)
	txMock := tx.On("Rollback")
	txMock.Return(nil)
	ctx, err = s.testRepo.Rollback(ctx)
	assert.Nil(s.T(), err)
	assert.Nil(s.T(), ctx.Value(transactionCtxKey))

	// failed to rollback
	ctx = context.WithValue(ctx, transactionCtxKey, tx)
	txMock.Return(fmt.Errorf("test error"))
	ctx, err = s.testRepo.Rollback(ctx)
	assert.NotNil(s.T(), err)
}

func TestBaseTestSuite(t *testing.T) {
	suite.Run(t, new(baseTestSuite))
}

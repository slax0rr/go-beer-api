/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package database

import (
	"context"

	"gitlab.com/slax0rr/go-beer-api/domain"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
)

type Address struct {
	*base
}

// NewAddress records the database connection instance to the new Address database object and returns it.
func NewAddress(db pgwrapper.DB) *Address {
	return &Address{&base{db}}
}

// Create inserts a new Country and City to the database.
func (p *Address) Create(ctx context.Context, c *domain.City) error {
	db := p.getDB(ctx)

	if c.Country != nil {
		_, err := db.Model(c.Country).Returning("iso2").Insert()
		if err != nil {
			return err
		}

		c.CountryISO2 = c.Country.ISO2
	}

	_, err := db.Model(c).Returning("id").Insert()
	if err != nil {
		return err
	}

	return nil
}

// Get all cities with the associated countries.
func (p *Address) Get(ctx context.Context) ([]domain.City, error) {
	var cities []domain.City

	err := p.getDB(ctx).Model(&cities).Relation("Country").Select()
	if err != nil {
		return nil, err
	}

	return cities, nil
}

// GetByCityID obtains the whole Address value object from the database by the city ID.
func (p *Address) GetByCityID(ctx context.Context, id int64) (*domain.City, error) {
	city := new(domain.City)

	err := p.getDB(ctx).Model(city).Relation("Country").WherePK().Select()
	if err != nil {
		return nil, err
	}

	return city, nil
}

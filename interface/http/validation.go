/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package http

import "github.com/go-playground/validator/v10"

type fieldError struct {
	Field     string      `json:"field"`
	Rule      string      `json:"rule"`
	Namespace string      `json:"namespace"`
	Value     interface{} `json:"value"`
}

type validationErrors []fieldError

func parseValidatorErrors(vldErr validator.ValidationErrors) validationErrors {
	err := make(validationErrors, len(vldErr))

	for i, e := range vldErr {
		err[i] = fieldError{
			Field:     e.Field(),
			Rule:      e.Tag(),
			Namespace: e.Namespace(),
			Value:     e.Value(),
		}
	}

	return err
}

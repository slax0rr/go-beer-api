/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package domain

import (
	"context"
	"fmt"

	"github.com/go-playground/validator/v10"
)

// Beer is the representation of the beer record in the database, and is used in requests/responses.
type Beer struct {
	tableName struct{} `pg:"beer"`

	ID             int64        `json:"id" pg:",pk,notnull"`
	Name           string       `json:"name" pg:",notnull,unique" validate:"required"`
	Alcohol        float64      `json:"alcohol" pg:",user_zero"`
	ManufacturerID int64        `json:"manufacturer_id,omitempty" pg:",notnull" validate:"required"`
	Manufacturer   Manufacturer `json:"manufacturer,omitempty" pg:",notnull" validate:"structonly"`

	Model
}

// Validate executes data validation of the Beer.
// With this, the http.IValidator interface is implemented, and the data is validated when the request is parsed.
func (b *Beer) Validate() validator.ValidationErrors {
	err := validator.New().Struct(b)
	if err != nil {
		return err.(validator.ValidationErrors)
	}

	return nil
}

// Save persists the Beer entity by either creating or updating the data.
func (b *Beer) Save(ctx context.Context) error {
	if b.ID == 0 {
		return beerRepoImpl.Create(ctx, b)
	}

	return beerRepoImpl.Update(ctx, b)
}

// Get a single beer from persistent storage.
// Removes the `ManufacturerID`, `CityID`, and `CountryISO2` values from the objects leaving only fully populated VO in the Beer entity.
func (b *Beer) Get(ctx context.Context) error {
	if b.ID <= 0 {
		return fmt.Errorf("unable to get beer, ID is zero or negative")
	}

	beer, err := beerRepoImpl.GetByID(ctx, b.ID)
	switch err {
	case nil:
		*b = *beer
		b.ManufacturerID = 0
		b.Manufacturer.CityID = 0
		b.Manufacturer.City.CountryISO2 = ""
		return nil

	default:
		return err
	}
}

// Delete beer from persistent storage.
func (b *Beer) Delete(ctx context.Context) error {
	return beerRepoImpl.Delete(ctx, b)
}

// BeerRepo lists required methods that the database object must implement in order to be usable by the Beer entity.
type BeerRepo interface {
	TransactionalRepo

	Create(context.Context, *Beer) error
	Update(context.Context, *Beer) error
	Delete(context.Context, *Beer) error
	Get(context.Context) ([]Beer, error)
	GetByID(context.Context, int64) (*Beer, error)
}

var beerRepoImpl BeerRepo

// InitBeerRepo sets the database object that implements the BeerRepo interface as the current implemenation used for database of the Beer entity.
func InitBeerRepo(impl BeerRepo) {
	beerRepoImpl = impl
}

// GetBeerRepo returns the current implementation used for database of the Beer entity.
func GetBeerRepo() BeerRepo {
	return beerRepoImpl
}

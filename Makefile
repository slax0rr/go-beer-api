GOCMD=go
CURL=curl
MOCKERY=mockery
PKG_PATH=gitlab.com/slax0rr/go-beer-api/cli
BIN_PATH=${GOPATH}/bin
CMD_NAME=beerapi
CFG_FILE=resources/config/beerapi.yaml
MOCK_DIR=./test/mock/
MOCK_PKG=mock

.PHONY: test

all: mocks test build run

mocks:
	${MOCKERY} \
		-dir=./application \
		-name=Manufacturer \
		-outpkg=${MOCK_PKG} \
		-output=${MOCK_DIR}

	${MOCKERY} \
		-dir=./application \
		-name=Beer \
		-outpkg=${MOCK_PKG} \
		-output=${MOCK_DIR}

	${MOCKERY} \
		-dir=./interface/http \
		-name=HTTPHelper \
		-outpkg=${MOCK_PKG} \
		-output=${MOCK_DIR}

test: mocks
	${GOCMD} test -v ./...

build:
	${GOCMD} mod tidy

	${GOCMD} build -o ${BIN_PATH}/${CMD_NAME} ${PKG_PATH}

run: build
	${BIN_PATH}/${CMD_NAME} --config ${CFG_FILE}

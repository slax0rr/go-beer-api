http:
  host: ''
  port: 3000
  sockFile: '/tmp/beerapi.sock'
  timeout: 5
  healthcheck: true # defines if the healthcheck route should be registered
db:
  enabled: true
  network: 'tcp' # tcp or unix
  host: '${DBHOST}'
  port: ${DBPORT}
  name: '${DBNAME}'
  user: '${DBUSER}'
  pass: '${DBPASS}'
  sslmode: 'disable' # default = require
  timeout: 0 # connection timeout in seconds - 0 = no timeout

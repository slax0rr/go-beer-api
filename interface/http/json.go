/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package http

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

type httpJSONHelperImpl struct{}

// NewHTTPJSONHelperImpl returns a new instance of the httpJSONHelperImpl
func NewHTTPJSONHelperImpl() *httpJSONHelperImpl {
	return new(httpJSONHelperImpl)
}

// SendResponse writes the `data` encoded as json to the `w` writer, and setting the appropriate headers as well as the response `status`.
// If `data` is nil, nothing is writen to the writer.
func (j *httpJSONHelperImpl) SendResponse(w http.ResponseWriter, data interface{}, status int) {
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(status)

	if data == nil {
		return
	}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.WithError(err).Error("unable to marshal success response")
	}
}

// SendError writes the `err` error to the `w` writer, setting the response status code as defined by the error object.
func (j *httpJSONHelperImpl) SendError(w http.ResponseWriter, err Error) {
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(err.Status)

	if err := json.NewEncoder(w).Encode(err); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.WithError(err).Error("unable to marshal success response")
	}
}

// IValidator indicates that the object provides a `Validate` method which is used for data validation.
type IValidator interface {
	// Validate the data in the struct and return any errors.
	Validate() validator.ValidationErrors
}

// ParseRequest attempts to decode the `req.Body` as JSON into the `data` object.
// If the `data` type impleements the `IValidator` interface, the data is validated using the objects `Validate` function.
// If the request data cannot be decoded or the validation fails, an error is written to the `w` writer.
func (j *httpJSONHelperImpl) ParseRequest(w http.ResponseWriter, req *http.Request, data interface{}) error {
	if err := json.NewDecoder(req.Body).Decode(data); err != nil {
		logrus.WithError(err).Error("unable to decode request as json")
		j.SendError(w, ErrBadRequest)
		return err
	}

	if v, ok := data.(IValidator); ok {
		if vldErr := v.Validate(); vldErr != nil {
			logrus.WithError(vldErr).Debug("request data invalid")

			err := ErrRequestDataInvalid
			err.Details = parseValidatorErrors(vldErr)
			j.SendError(w, err)
			return vldErr
		}
	}

	return nil
}

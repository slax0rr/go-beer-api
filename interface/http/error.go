/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package http

import "net/http"

type Error struct {
	Status  int         `json:"-"`
	Code    string      `json:"error"`
	Message string      `json:"error_message"`
	Details interface{} `json:"details,omitempty"`
}

func (err Error) Error() string {
	return err.Message
}

var (
	ErrBadRequest Error = Error{
		Status:  http.StatusBadRequest,
		Code:    "bad_request",
		Message: "The server cannot process the request",
	}

	ErrNotFound = Error{
		Status:  http.StatusNotFound,
		Code:    "not_found",
		Message: "The server could not find the requested resource",
	}

	ErrConflict = Error{
		Status:  http.StatusConflict,
		Code:    "conflict",
		Message: "The server could not process the request due to a conflict in the current state",
	}

	ErrRequestDataInvalid = Error{
		Status:  http.StatusUnprocessableEntity,
		Code:    "validation_error",
		Message: "The request data is not in a valid form",
	}

	ErrInternalServerError = Error{
		Status:  http.StatusInternalServerError,
		Code:    "internal_error",
		Message: "An unexpected error has occured",
	}
)

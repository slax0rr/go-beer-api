/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package handlers

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/go-beer-api/application"
	"gitlab.com/slax0rr/go-beer-api/domain"
	"gitlab.com/slax0rr/go-beer-api/infrastructure/database"
	httphelper "gitlab.com/slax0rr/go-beer-api/interface/http"
)

type Manufacturer struct {
	manufacturerApp application.Manufacturer
	http            httphelper.HTTPHelper
}

// NewManufacturerHandler is the factory for creating the Manufacturer handler.
func NewManufacturerHandler(
	manufacturerApp application.Manufacturer,
	http httphelper.HTTPHelper,
) *Manufacturer {
	return &Manufacturer{manufacturerApp, http}
}

// Register registers the handler endpoints to the router.
func (h *Manufacturer) Register(rtr *mux.Router) {
	r := rtr.PathPrefix("/manufacturer").Subrouter()

	r.Path("").HandlerFunc(h.Create).Methods(http.MethodPost)
	r.Path("").HandlerFunc(h.List).Methods(http.MethodGet)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Get).Methods(http.MethodGet)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Update).Methods(http.MethodPut)
	r.Path("/{id:[0-9]+}").HandlerFunc(h.Delete).Methods(http.MethodDelete)
}

// Create handles the POST /manufacturer endpoint by parsing the incoming request and passing the `domain.Manufacturer` entity object on to the ManufacturerApp.
func (h *Manufacturer) Create(w http.ResponseWriter, req *http.Request) {
	manufacturer := new(domain.Manufacturer)
	if err := h.http.ParseRequest(w, req, manufacturer); err != nil {
		return
	}

	err := h.manufacturerApp.Create(req.Context(), manufacturer)
	if err != nil {
		logrus.WithError(err).Error("unable to create a new manufacturer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}

	h.http.SendResponse(w, manufacturer, http.StatusCreated)
}

// List handles the GET /manufacturer endpoint by providing a list of all available `domain.Manufacturer` objects.
func (h *Manufacturer) List(w http.ResponseWriter, req *http.Request) {
	list, err := h.manufacturerApp.List(req.Context())
	if err != nil {
		logrus.WithError(err).Error("unable to obtain a list of manufacturers")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}

	h.http.SendResponse(w, list, http.StatusOK)
}

// Get handles the GET /manufacturer/{id} endpoint by obtaining the requested `domain.Manufacturer` object.
func (h *Manufacturer) Get(w http.ResponseWriter, req *http.Request) {
	// an error can not occur, since the regex in the path already limits to positive numbers
	id, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	manufacturer, err := h.manufacturerApp.GetByID(req.Context(), id)
	switch {
	case err == nil:
		h.http.SendResponse(w, manufacturer, http.StatusOK)

	case err.Error() == database.ErrRowNotFound.Error():
		logrus.WithField("error", err).Info("requested manufacturer not found")
		h.http.SendError(w, httphelper.ErrNotFound)
		return

	default:
		logrus.WithError(err).WithField("id", id).Error("unable to obtain the manufacturer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

// Update handles the PUT /manufacturer/{id} endpoint by updating the `domain.Manufacturer` object.
func (h *Manufacturer) Update(w http.ResponseWriter, req *http.Request) {
	manufacturer := new(domain.Manufacturer)
	if err := h.http.ParseRequest(w, req, manufacturer); err != nil {
		return
	}

	// an error can not occur, since the regex in the path already limits to positive numbers
	manufacturer.ID, _ = strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	err := h.manufacturerApp.Update(req.Context(), manufacturer)
	switch {
	case err == nil:
		h.http.SendResponse(w, manufacturer, http.StatusOK)

	case err.Error() == database.ErrRowNotFound.Error():
		logrus.WithField("error", err).Info("requested manufacturer not found")
		h.http.SendError(w, httphelper.ErrNotFound)
		return

	default:
		logrus.WithError(err).
			WithField("id", manufacturer.ID).
			Error("unable to update the manufacturer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

// Delete handles the DELETE /manufacturer/{id} endpoint by removing the `domain.Manufacturer` object.
func (h *Manufacturer) Delete(w http.ResponseWriter, req *http.Request) {
	// an error can not occur, since the regex in the path already limits to positive numbers
	id, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)

	err := h.manufacturerApp.Delete(req.Context(), id)
	switch {
	case err == nil:
		fallthrough

	case err.Error() == database.ErrRowNotFound.Error():
		h.http.SendResponse(w, nil, http.StatusNoContent)

	case err.Error() == database.ErrForeignKeyConstraint.Error():
		logrus.WithError(err).
			WithField("id", id).
			Error("unable to delete the manufacturer")
		respErr := httphelper.ErrConflict
		respErr.Details = err
		h.http.SendError(w, respErr)

	default:
		logrus.WithError(err).
			WithField("id", id).
			Error("unable to delete the manufacturer")
		h.http.SendError(w, httphelper.ErrInternalServerError)
		return
	}
}

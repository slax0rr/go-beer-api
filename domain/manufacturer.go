/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package domain

import (
	"context"
	"fmt"

	"github.com/go-playground/validator/v10"
)

// Manufacturer is the representation of the beer manufacturer record in the database, and is used in requests/responses.
type Manufacturer struct {
	tableName struct{} `pg:"manufacturer"`

	ID      int64  `json:"id" pg:",pk,notnull"`
	Name    string `json:"name" pg:",notnull,unique" validate:"required"`
	Street1 string `json:"street1" pg:",notnull" validate:"required"`
	Street2 string `json:"street2"`
	ZipCode string `json:"zip_code" pg:",notnull" validate:"required"`
	CityID  int64  `json:"city_id,omitempty" pg:",notnull" validate:"required_without=City"`
	City    *City  `json:"city,omitempty" pg:",notnull" validate:"required_without=CityID"`

	Model
}

// Validate executes data validation of the Manufacturer.
// With this, the http.IValidator interface is implemented, and the data is validated when the request is parsed.
func (m *Manufacturer) Validate() validator.ValidationErrors {
	err := validator.New().Struct(m)
	if err != nil {
		return err.(validator.ValidationErrors)
	}

	return nil
}

// Save persists the Manufacturer entity by either creating or updating the data.
func (m *Manufacturer) Save(ctx context.Context) error {
	if m.ID == 0 {
		ctx, err := manufacturerRepoImpl.Begin(ctx)
		if err != nil {
			return err
		}
		defer manufacturerRepoImpl.Rollback(ctx)

		if m.CityID == 0 {
			err = m.City.Save(ctx)
			if err != nil {
				return err
			}

			m.CityID = m.City.ID
		}

		err = manufacturerRepoImpl.Create(ctx, m)
		if err != nil {
			return err
		}

		_, err = manufacturerRepoImpl.Commit(ctx)
		if err != nil {
			return err
		}

		return nil
	}

	return manufacturerRepoImpl.Update(ctx, m)
}

// Get a single manufacturer from persistent storage.
// Removes the `CityID` and `CountryISO2` values from the objects leaving only fully populate VO in the Manufacturer entity.
func (m *Manufacturer) Get(ctx context.Context) error {
	if m.ID <= 0 {
		return fmt.Errorf("unable to get manufacturer, ID is zero or negative")
	}

	manufacturer, err := manufacturerRepoImpl.GetByID(ctx, m.ID)
	switch err {
	case nil:
		*m = *manufacturer
		m.CityID = 0
		m.City.CountryISO2 = ""
		return nil

	default:
		return err
	}
}

// Delete manufacturer from persistent storage.
func (m *Manufacturer) Delete(ctx context.Context) error {
	return manufacturerRepoImpl.Delete(ctx, m)
}

// ManufacturerRepo lists required methods that the database object must implement in order to be usable by the Manufacturer entity.
type ManufacturerRepo interface {
	TransactionalRepo

	Create(context.Context, *Manufacturer) error
	Update(context.Context, *Manufacturer) error
	Delete(context.Context, *Manufacturer) error
	Get(context.Context) ([]Manufacturer, error)
	GetByID(context.Context, int64) (*Manufacturer, error)
}

var manufacturerRepoImpl ManufacturerRepo

// InitManufacturerRepo sets the database object that implements the ManufacturerRepo interface as the current implementation used for database of the Manufacturer entity.
func InitManufacturerRepo(impl ManufacturerRepo) {
	manufacturerRepoImpl = impl
}

// GetManufacturerRepo returns the current implementation used for database of the Manufacturer entity.
func GetManufacturerRepo() ManufacturerRepo {
	return manufacturerRepoImpl
}

module gitlab.com/slax0rr/go-beer-api

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-pg/pg/v9 v9.1.5
	github.com/go-pg/urlstruct v0.4.0 // indirect
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/slax0rr/go-httpserver v0.0.0-20191215144154-45b9896ca1d2
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5 // indirect
	github.com/vmihailenco/bufpool v0.1.6 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.11 // indirect
	gitlab.com/slax0rr/go-pg-wrapper v0.0.0-20200404072502-52992ef0fb64
	golang.org/x/crypto v0.0.0-20200403201458-baeed622b8d8 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
)

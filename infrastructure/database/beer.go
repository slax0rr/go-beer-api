/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package database

import (
	"context"
	"strconv"

	"gitlab.com/slax0rr/go-beer-api/domain"
	pgwrapper "gitlab.com/slax0rr/go-pg-wrapper"
)

// Beer database struct, holding the database connection instance.
type Beer struct {
	*base
}

// NewBeer records the database connection instance to the new Beer database object and returns it.
func NewBeer(db pgwrapper.DB) *Beer {
	return &Beer{&base{db}}
}

// Create a new Beer in the database table.
func (p *Beer) Create(ctx context.Context, m *domain.Beer) error {
	_, err := p.getDB(ctx).Model(m).Returning("id").Insert()
	if err != nil {
		return err
	}

	return nil
}

// Update the beer record in the database.
// If the beer is not found the `ErrRowNotFound` error is returned.
func (p *Beer) Update(ctx context.Context, m *domain.Beer) error {
	res, err := p.getDB(ctx).Model(m).
		Column("name", "alcohol", "manufacturer_id", "updated_at").
		WherePK().
		Update()
	if err != nil {
		return err
	}

	if res.RowsAffected() == 0 {
		persErr := ErrRowNotFound
		persErr.Details = struct{ ID int64 }{m.ID}
		return persErr
	}

	return nil
}

// Delete the beer record from the database.
func (p *Beer) Delete(ctx context.Context, m *domain.Beer) error {
	res, err := p.getDB(ctx).Model(m).WherePK().Delete()
	switch e := parseError(err).(type) {
	case nil:

	case Error:
		e.Details.(map[string]string)["id"] = strconv.FormatInt(m.ID, 10)
		return e

	default:
		return e
	}
	if err != nil {
		return parseError(err)
	}

	if res.RowsAffected() == 0 {
		persErr := ErrRowNotFound
		persErr.Details = struct{ ID int64 }{m.ID}
		return persErr
	}

	return nil
}

// Get all beers from the database.
func (p *Beer) Get(ctx context.Context) ([]domain.Beer, error) {
	var beers []domain.Beer

	err := p.getDB(ctx).Model(&beers).Relation("Manufacturer").Select()
	if err != nil {
		return nil, err
	}

	return beers, nil
}

// GetByID returns a single beer from the database.
func (p *Beer) GetByID(ctx context.Context, id int64) (*domain.Beer, error) {
	m := new(domain.Beer)

	err := p.getDB(ctx).
		Model(m).
		Relation("Manufacturer").
		Relation("Manufacturer.City").
		Relation("Manufacturer.City.Country").
		Where("beer.id = ?", id).
		Select()
	if err != nil {
		return nil, err
	}

	return m, nil
}

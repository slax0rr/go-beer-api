/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package application

import (
	"context"
	"fmt"

	"gitlab.com/slax0rr/go-beer-api/domain"
)

type Beer interface {
	Create(context.Context, *domain.Beer) error
	Update(context.Context, *domain.Beer) error
	Delete(context.Context, int64) error
	List(context.Context) ([]domain.Beer, error)
	GetByID(context.Context, int64) (*domain.Beer, error)
}

// Beer application layer between the intarface and the domain implementation of beer.
type BeerImpl struct{}

// Compile-time check if BeerImpl implements Beer
var _ Beer = new(BeerImpl)

// Create a new beer object.
func (a *BeerImpl) Create(ctx context.Context, beer *domain.Beer) error {
	if beer == nil {
		return fmt.Errorf("beer entity may not be nil")
	}

	return beer.Save(ctx)
}

// Update an existing beer object.
func (a *BeerImpl) Update(ctx context.Context, beer *domain.Beer) error {
	if beer == nil {
		return fmt.Errorf("beer entity may not be nil")
	}

	if beer.ID == 0 {
		return ErrZeroID
	}

	return beer.Save(ctx)
}

// Delete an existing beer object.
func (a *BeerImpl) Delete(ctx context.Context, id int64) error {
	beer := new(domain.Beer)
	beer.ID = id

	return beer.Delete(ctx)
}

// List obtains a list of beers from the domain and returns them.
func (a *BeerImpl) List(ctx context.Context) ([]domain.Beer, error) {
	return domain.GetBeerRepo().Get(ctx)
}

// GetByID obtains a single beer with the specified `id` and returns it.
func (a *BeerImpl) GetByID(ctx context.Context, id int64) (*domain.Beer, error) {
	beer := new(domain.Beer)
	beer.ID = id

	err := beer.Get(ctx)
	if err != nil {
		return nil, err
	}

	return beer, nil
}

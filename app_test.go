/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package beerapi_test

import (
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/spf13/viper"
	beerapi "gitlab.com/slax0rr/go-beer-api"
)

// Simply start the server and ensure the server is running.
func TestServerSpinup(t *testing.T) {
	sockFile := "/tmp/beerapi-test.sock"

	viper.Set("http.sockFile", sockFile)
	viper.Set("http.host", "")
	viper.Set("http.port", 9999)
	viper.Set("http.timeout", 5)
	viper.Set("http.healthcheck", true)

	// Run it in a separate routine
	go beerapi.Run()

	// remove socket file after execution
	defer func() {
		err := os.Remove(sockFile)
		if err != nil {
			t.Logf("sock file was not removed due to an error: %s", err)
		}
	}()

	// wait for the server to spin up
	time.Sleep(500 * time.Millisecond)

	resp, err := http.DefaultClient.Get("http://127.0.0.1:9999/healthcheck")
	if err != nil {
		t.Errorf("unable to do a healthcheck: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("unexpected healthcheck statuscode: %d", resp.StatusCode)
	}
}

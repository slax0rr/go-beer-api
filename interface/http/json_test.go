/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package http

import (
	"bytes"
	"encoding/json"
	stdhttp "net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
)

func TestSendResponse(t *testing.T) {
	testCase := struct {
		input  map[string]string
		output []byte
		status int
	}{
		input:  map[string]string{"foo": "bar"},
		status: 999,
	}

	w := bytes.NewBuffer(nil)
	err := json.NewEncoder(w).Encode(testCase.input)
	if err != nil {
		t.Fatalf("unable to encode test data: %s", err)
	}
	testCase.output = w.Bytes()

	rec := httptest.NewRecorder()
	helper := NewHTTPJSONHelperImpl()
	helper.SendResponse(rec, testCase.input, testCase.status)

	assert.Equal(t, testCase.status, rec.Code)
	assert.Equal(t, string(testCase.output), rec.Body.String(), "SendResponse did not produce an expected response")
}

func TestSendError(t *testing.T) {
	testCase := struct {
		input  Error
		output []byte
	}{
		input: Error{Status: 999, Code: "foo", Message: "bar", Details: "baz"},
	}

	w := bytes.NewBuffer(nil)
	err := json.NewEncoder(w).Encode(testCase.input)
	if err != nil {
		t.Fatalf("unable to encode test data: %s", err)
	}
	testCase.output = w.Bytes()

	rec := httptest.NewRecorder()
	helper := NewHTTPJSONHelperImpl()
	helper.SendError(rec, testCase.input)

	assert.Equal(t, testCase.input.Status, rec.Code)
	assert.Equal(t, string(testCase.output), rec.Body.String(), "SendError did not produce an expected response")
}

type foo struct {
	Foo string `json:"foo" validate:"eq=bar"`
}

func (f foo) Validate() validator.ValidationErrors {
	err := validator.New().Struct(f)
	if err != nil {
		return err.(validator.ValidationErrors)
	}
	return nil
}

func TestParseRequest(t *testing.T) {
	badReqJSON := bytes.NewBuffer(nil)
	err := json.NewEncoder(badReqJSON).Encode(ErrBadRequest)
	if err != nil {
		t.Fatalf("unable to marshal bad request error as JSON: %s", err)
	}

	vldErr := ErrRequestDataInvalid
	vldErr.Details = parseValidatorErrors(foo{Foo: "baz"}.Validate())
	validErrJSON := bytes.NewBuffer(nil)
	err = json.NewEncoder(validErrJSON).Encode(vldErr)
	if err != nil {
		t.Fatalf("unable to marshal validation error as JSON: %s", err)
	}

	testCases := []struct {
		input  []byte
		val    string
		output []byte
		status int
	}{
		{
			input:  []byte(`{"foo":"bar"}`),
			val:    "bar",
			output: nil,
		},
		{
			input:  []byte(`this is not json`),
			output: badReqJSON.Bytes(),
			status: 400,
		},
		{
			input:  []byte(`{"foo":"baz"}`),
			output: validErrJSON.Bytes(),
			status: 422,
		},
	}

	for _, tc := range testCases {
		req, err := stdhttp.NewRequest("POST", "/foo", bytes.NewBuffer(tc.input))
		if err != nil {
			t.Fatalf("unable to create test request: %s", err)
		}

		rec := httptest.NewRecorder()
		helper := NewHTTPJSONHelperImpl()
		f := foo{}
		helper.ParseRequest(rec, req, &f)

		if tc.status > 0 {
			assert.Equal(t, tc.status, rec.Code)
			assert.Equal(t, string(tc.output), rec.Body.String(), "ParseRequest did not produce an expected response")
		}

		if tc.val != "" {
			assert.Equal(t, tc.val, f.Foo)
		}
	}
}
